# README #

More info about project is on [Wiki](https://bitbucket.org/jollycoopteam/epics-repo/wiki/Home)

### What is this repository for? ###

* Image uploader and image sharing website
* 0.0.1

### How do I get set up? ###

* Summary of set up - Just run it in visual studio. Make sure that UI.WebSite.WebHost project set to startup. Make sure you`ve cloned **dev branch**.
* Configuration - none for now.
* Dependencies .Net framework 4.5. or higher.
* Database configuration - none for now
* How to run tests - none for now
* Deployment instructions -none for now

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact